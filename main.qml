import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import myTimer 1.0
Window {
    visible: true
    width: Screen.width -1
    height: Screen.height -1
    id: roots

    //Trang thai chay cua trang
    property bool isStart: false
    //luu tru hinh anh nen (background)
    property var background: "images/backgroundImage.png"
    //Toa do mac dinh cua sticker khi duoc chon ra man hinh
    property double xCoorDefault: roots.width/2
    property double yCoorDefault: roots.height/2
    //Chi so dau tien cua cac sticker chon ra man hinh
    property var firstIndexSticker: []
    //mang luu tru tam thoi cac toa do
    property var arrayCoors: []
    //trang thai hien thi cua mui ten chon toa do di chuyen
    property bool isVisibleArrow: false
    property bool tickVisible: false
    property int  tickNum: 0
    //Tao listmodel de luu tru hinh anh va toa do cua hinh anh do
    Rectangle{
        ListModel{
            id: stickerModels
            ListElement{
                SrcSticker: [ListElement{src: ""}]
                Coors: [
                    ListElement{
                        x: 0
                        y: 0
                    }
                ]
                imgFeature: ""
            }
        }
        //Dung de danh dau khi ma mui ten toa do click vao
        ListModel{ id: tickModels }

        //Mo file de chon back ground
        FileDialog{
            id: bgDialog
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            onAccepted: {
                background = bgDialog.fileUrl
            }
        }
        //Mo file de chon Sticker
        FileDialog{
            id: stickerDialog
            nameFilters: ["*.png", "*.jpg", "All file(*)"]
            folder: shortcuts.music+"/bibi_data/characters/thumbnail"
            selectMultiple: clickStick.acceptedButtons
            onAccepted: {
                isChooseSticker()
            }
        }

        //Background image
        Image {
            anchors.top: tools.bottom
            id: backgroundImage
            source: background
            width: roots.width
            height: roots.height-bgIcon.height
        }
        Row{
            id: tools
            //Icon background
            Image {
                id: bgIcon
                source: "images/bgicon.png"
                width: Screen.width/20
                height: Screen.height/12

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bgDialog.open()
                    }
                }
            }
            //Icon Sticker
            Image {
                id: stickerIcon
                source: "images/sticker.png"
                width: bgIcon.width
                height: bgIcon.height

                MouseArea{
                    id: clickStick
                    anchors.fill: parent
                    onClicked: {
                        stickerDialog.open()
                        stickerDialog.selectMultiple
                    }
                }
            }
            //Icon start and stop
            Image {
                id: startStop
                source: !isStart ? "images/start1.png" : "images/stop.png"
                width: bgIcon.width
                height: bgIcon.height
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        isStart = !isStart
                        if(isStart){
                            actionTimer.start()
                        }else if(!isStart){
                            actionTimer.stop()
                        }
                    }
                }
            }
            //Mui ten chon toa do
            Image {
                id: arrowIcon
                source: (!isVisibleArrow) ? "" : "images/arrow2.png"
                width: bgIcon.width*3/4
                height: bgIcon.height/2
                y: bgIcon.height/2 - height/2
                Drag.active: dragArr.drag.active
                Drag.hotSpot.x: arrowIcon.x
                Drag.hotSpot.y: arrowIcon.y
                MouseArea{
                    id: dragArr
                    anchors.fill: parent
                    drag.target: parent
                    onClicked: pushCoors() //thuc hien push toa do vao mang
                }
            }

        }
        //Listview hien thi danh sach cac sticker minh chon
        ListView{
            id: listViewSticker
            model: stickerModels
            delegate: Rectangle{
                Image {
                    id: stickerImg
                    source: imgFeature
                    width: 100
                    height: 150
                    x: roots.width/2
                    y: roots.height/2
                    Drag.active: clickSticker.drag.active
                    Drag.hotSpot.x: stickerImg.x
                    Drag.hotSpot.y: stickerImg.y
                    MouseArea{
                        id: clickSticker
                        anchors.fill: parent
                        drag.target: parent
                        onClicked: {
                            isVisibleArrow = !isVisibleArrow
                            tickModels.clear()
                            tickNum = 0
                        }
                    }
                }
                //animation cho sticker
                ParallelAnimation{
                    id: animateSticker

                    NumberAnimation {
                        target: stickerImg
                        property: "x"
                        //to:
                        duration: 200
                        easing.type: Easing.InOutQuad
                    }
                }

                Button{
                    id: updateCoors
                    text: "update tọa độ"
                    x: tools.width
                    y: bgIcon.height/2 - height/2
                    onClicked: {
                        if(arrayCoors.length > 0){
                            stickerModels.set(index, {Coors: arrayCoors})
                            arrayCoors = []
                        }
                        console.log("toa do sau khi update---------")
                        for(var i = 0; i < stickerModels.get(index).Coors.count; i++){
                            console.log("("+
                                    stickerModels.get(index).Coors.get(i).x + ", "+
                                        stickerModels.get(index).Coors.get(i).y+ ")")
                        }

                        //Lay chi so cua icon update
                        console.log("chi so: "+index)
                    }
                }
            }
        }
        ListView{
            id: tick
            model: tickModels
            delegate: Rectangle{
                visible: tickVisible
                Text{
                    text: "name"
                    color: "red"
                    x: x
                    y: y
                }
            }
        }

        //Qtimer set thoi gian chuyen dong cua sticker de tao trang thai chuyen dong
        QTimer{
            id: actionTimer
            interval: 100
            onTimeout: {
                for(var i = 0; i < stickerModels.count; i++){
                    if(firstIndexSticker[i] < stickerModels.get(i).SrcSticker.count){
                        stickerModels.set(i, {imgFeature: stickerModels.get(i).SrcSticker.get(firstIndexSticker[i]).src})
                        firstIndexSticker[i]++
                    }else firstIndexSticker[i] = 0
                }
            }
        }
    }
    function isChooseSticker(){
        console.log("you choose file: "+stickerDialog.fileUrls)
        var arrUrls = [], listUrls = []
        arrUrls = stickerDialog.fileUrls
        var featured ;
        for(var i = 0; i < arrUrls.length; i++){
            featured = arrUrls[0]
            listUrls.push({src: arrUrls[i]})
        }
        var coors = [{x: xCoorDefault, y: yCoorDefault}]
        stickerModels.append({
                                SrcSticker: listUrls,
                                Coors: coors,
                                imgFeature: featured
                             })
    }

    //lay toa do va day vao models
    function pushCoors(){
        arrayCoors.push({x: arrowIcon.x, y: arrowIcon.y})
        tickModels.append({x: arrowIcon.x, y: arrowIcon.y, name: tickNum++})
        tickVisible = true
        console.log(tickModels.count)
    }
}
